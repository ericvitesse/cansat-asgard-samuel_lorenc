/*
   CCS811_Client.h
*/
#pragma once

#include "IsaTwoRecord.h"
#include "Adafruit_CCS811.h"

class CCS811_Client {

  public:
    /** Start CCS811 sensor, using an analog pin as wake-up line. If this version
     * 	of the begin() is used, it is assumed that the wake pin is permanently
     * 	pulled down, and no action will be taken in further calls to readData()
     * 	to wake up the sensor.
     *  @return true if everything ok, false otherwise.
     */
    bool begin();
    /** Start CCS811 sensor, using an analog pin as wake-up line. If this version
     * 	of the begin() is used, every call to readData() will make use of the
     * 	provided analog pin to wake up the sensor.
     *  @param theAnalogWakePin  The number of the analog pin used for the wake.
     *  @return true if everything ok, false otherwise.
     */
    bool beginUsingAnalogWakePin(byte theAnalogWakePin);

    /** Read data from the sensor, if any available. If none is available, the
     *  value is set to 0 in the record.
     *  @param The record in which reading must be stored if any
     *  @return True if a reading was available, false otherwise.
     */
    bool readData(IsaTwoRecord& rec);

    protected:
    /** Pull the sensor's wake line up or down. The controller's pin used (if any)
     *  is defined by the version of the begin() function that was used.
     *  @param status If true, wake-up the sensor, if false put it to sleep.
     */
    void wakeSensor(bool status);
  private:
    Adafruit_CCS811 CCS811;
    byte analogWakePin; /**<  The analog pin to use to wake the sensor. 255 if no
    					 	  analog pin is used. */
    
};
