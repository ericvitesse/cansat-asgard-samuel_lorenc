#include "CCS811_Client.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG 0

bool CCS811_Client::begin() {
  analogWakePin=255;
  if (!CCS811.begin()) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Failed to start the CO2 sensor!");
    return false;
  }
  return true;
}

bool CCS811_Client::beginUsingAnalogWakePin(byte theAnalogWakePin) {
  analogWakePin=theAnalogWakePin;
  wakeSensor(true);
  bool result = begin();
  wakeSensor(false);
  return result;
}

bool CCS811_Client::readData(IsaTwoRecord& rec){
  bool result=false;
  wakeSensor(true);
  if(CCS811.available()){
	CCS811.readData();
	DPRINTS (DBG, "CCS811 reading available (ppm): ");
	DPRINTLN(DBG, CCS811.geteCO2());
    rec.co2 = CCS811.geteCO2();
    result=true;
  } else {
	  rec.co2=0;
      DPRINTSLN(DBG, "No CCS811 reading available.");
  }
  wakeSensor(false);
  return result;
}

void CCS811_Client::wakeSensor(bool status) {
	if (analogWakePin != 255) {
		// Use the analog line to wake up sensor
		auto value=(status ? 0 : 255);
		DPRINTS(DBG, "Setting CCS811 wake analog pin to ");
		DPRINTLN(DBG, value)
		analogWrite(analogWakePin, value);
	}
	// We could handle a digital wake pin here, after a corresponding begin() is
	// implemented.
}
