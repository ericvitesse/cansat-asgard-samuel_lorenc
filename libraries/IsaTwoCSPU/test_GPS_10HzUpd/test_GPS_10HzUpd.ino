/*
   ************ TO DO: COMPLETE AND THIS WITH FEATHER !!! ******
   * **************************************************************
   Connect the GPS Power pin to 5V
   If using Arduino Uno
      Connect the GPS Ground pin to ground
      Connect the GPS TX (transmit) pin to Digital 3
     Connect the GPS RX (receive) pin to Digital 2
   If using Feather M0 Express or any board with hardware Serial1:
     Connect the GPS TX (transmit) pin to RX
     Connect the GPS RX (receive) pin to TX
*/

#define DEBUG
#include "DebugCSPU.h"
#include <Adafruit_GPS.h>

constexpr bool GPSECHO = false;

#if defined(UBRR1H) || defined(HAVE_HWSERIAL1)  || defined(__SAMD21G18A__)
#  error This software is only intended to use on Arduino boards.
   // In case an hardware serial port is available use it
   // This covers the case of the Feather M0 Express
   Adafruit_GPS GPS(&Serial1);
#else
   // For Arduino Uno, use software serial.
#  include <SoftwareSerial.h>
   SoftwareSerial mySerial(3, 2);
   Adafruit_GPS GPS(&mySerial);
#endif

// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
#ifdef UDR0
  if (GPSECHO)
    if (c) UDR0 = c;
  // writing direct to UDR0 is much much faster than Serial.print
  // but only one character can be written at a time.
#endif
}

void setup() {
  DINIT(115200);
  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_BAUD_57600); // Change GPS baud to 38400
  // Same as above... GPS.sendCommand("$PMTK251,38400*27<CR><LF>");
  GPS.begin(57600);   // Switch serial communication to new baudrate.
  delay(250); //Pause
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ); // 10 Hz update rate
  // Similar to above: GPS.sendCommand("$PMTK220,100*2F<CR><LF>");
  Serial.println("Finished Setup");

  // install the interrupt routine.
  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function above
  // Info to read about timer interruptions:
  //    https://learn.adafruit.com/multi-tasking-the-arduino-part-2/timers
  //    http://www.carbonatethis.com/arduino-interrupts-and-timers/
  //    http://www.engblaze.com/microcontroller-tutorial-avr-and-arduino-timer-interrupts/
  OCR0A = 0xAF; // This defines that the interruption will be raised each time the counter
  // passes 0xAF; Counter is 8 bits, overflows at a frequency of about 1kHz.
  TIMSK0 |= _BV(OCIE0A);
  // TIMSK0=Timer Interrupt MaSK register for timer 0
  // OCIE0A=bit to enable Timer Compare interrupt.
  // _BV(xxx) is equivalent to 1 << xxx which should be preferred
  //          for readability

  delay(500); //Pause
}

void loop() {
  if (GPS.newNMEAreceived()) {
    Serial << "NMEAreceived at " << millis() << " ";
    if (GPS.parse(GPS.lastNMEA())) {
      Serial << GPS.hour << ":" << GPS.minute << ":" << GPS.seconds << "." << GPS.milliseconds;
      if (GPS.fix) {
        Serial << " FIX";  
      }
      else {
        Serial << " no fix";
      }
      Serial << ENDL;
      Serial << GPS.lastNMEA() << ENDL;
    }
    else {
      Serial << "Error in parsing: " << GPS.lastNMEA() << ENDL;
    }

  }
  //else Serial << ".";
}
