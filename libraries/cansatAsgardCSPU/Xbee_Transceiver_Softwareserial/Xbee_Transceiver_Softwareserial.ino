
const int RF_rxPinOnUNO = 9;
const int RF_TxPinOnUno = 11;

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#else
#  include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#endif

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  RF.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  RF.print('A');
  Serial.println("sent A");
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  RF.print ('B');
  Serial.println("sent B");
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
}
