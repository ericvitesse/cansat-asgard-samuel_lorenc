/*
   Test for class StorageManager
*/
#define DEBUG
#define USE_ASSERTIONS
#include "SD_Logger.h"

#include "DebugCSPU.h"

#include "StorageManager.h"
#include "EEPROM_BankWithTools.h"

const byte chipSelectPinNumber = 4;


typedef struct MyData {
  int a;
  float b;
} MyData;

const EEPROM_BankWriter::EEPROM_Key EEPROM_KeyValue = 0x1234;
HardwareScanner hw;
StorageManager sm(sizeof(MyData), 2, 3 );
MyData md;

void clearEEPROM()
{
  // First clear EEPROM
  EEPROM_Bank bank(5);
  bank.init(EEPROM_KeyValue, hw, sizeof(MyData));
  bank.erase();
}

void checkData() {
  // Check the content of the EEPROM
  EEPROM_Bank bank(5);
  bank.init(EEPROM_KeyValue, hw, sizeof(MyData));
  Serial << F("Found ") << bank.recordsLeftToRead() << F(" records in EEPROM") << ENDL;
  DASSERT(bank.recordsLeftToRead() == 2);

  md.a = 0;
  md.b = 0;
  bank.readOneRecord((byte* )&md, sizeof(md));
  DASSERT(md.a == 5);
  DASSERT(md.b == 3.141592);
  bank.readOneRecord((byte*) &md, sizeof(md));
  DASSERT(md.a == 6);
  DASSERT(md.b == 3.141592);

}
//----------------------------------------------------------------//
void setup() {
  DINIT(9600);
  Serial << F("testing StorageManager...") << ENDL;
  Serial << F("Size of StorageManager:") << sizeof(StorageManager) << ENDL;
  //delay(5)

  Serial << F("FreeRam: ") << freeRam() << ENDL;


  hw.init(1, 127, 0, 400);
  clearEEPROM();

  String msg = "dummy first line";
  bool smOK = sm.init(&hw, "abcd", chipSelectPinNumber, 100, EEPROM_KeyValue, msg);
  Serial << F("StorageManager::init() OK: ") << smOK << ENDL;
  Serial << F("Size of storageManager: ") << sizeof(sm) << F(" bytes") << ENDL;

  //init of md
  md.a = 5;
  md.b = 3.141592;
  //init of aString
  String aString = F("Bonjour");

  Serial << F("Testing storeOneRecord()...") << ENDL;
  Serial << F("FreeRam: ") << freeRam() << ENDL;
  sm.storeOneRecord((const byte*)&md, aString);
  md.a++;
  sm.storeOneRecord((const byte*)&md, aString);

  Serial << F("calling doIdle() during 10 secs...") << ENDL;
  for (int i = 0; i < 20 ; i++ ) {
    sm.doIdle();
    Serial << ".";
    delay(500);
  }
  Serial << ENDL << F("Checking data in EEPROM...") << ENDL;

  if (smOK) {
    checkData();
  }

  Serial << F("end of tests") << ENDL;
}

void loop() {
  // No loop

}

