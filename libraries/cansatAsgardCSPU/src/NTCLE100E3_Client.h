/*
   NTCLE100E3_Client.h
*/

#pragma  once
#include "ThermistorClient.h"

/**
 * @brief This a subclass of thermistorClient customized for thermistor NTCLE100E3223*BO,123*BO,153*BO.
 * Use this class to read thermistor resistance and convert to degrees.
 * wiring VCC to thermistor, thermistor to serialresistor, serialresistor to ground.

 */
class NTCLE100E3_Client: public ThermistorClient {
  public:
  /**
   * @param theAnalogPinNbr this is the pin of the card in which we put the cable to read the resistance.
   * @param theSerialResistor the second resistance we have put in the breadboard, in this case this is 10000ohms.
   */
    NTCLE100E3_Client(byte theAnalogPinNbr, float theSerialResistor ):
             ThermistorClient(theAnalogPinNbr, 22000.0, 3.354016E-03,  2.744032E-04, 3.666944E-06, 1.375492E-07, theSerialResistor) {};

};
