/*
   ThemistorClient.cpp
*/
#include "ThermistorClient.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG_READ  0

#ifdef ARDUINO_AVR_UNO
constexpr float Vmax = 5.0;
constexpr uint16_t Nsteps = 1024;
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
constexpr float Vmax = 3.3;
constexpr uint16_t Nsteps = 4096 ;
#else
#error "Board not recognised"
#endif

ThermistorClient::ThermistorClient(const uint8_t theAnalogPinNbr, const float theRefResistor, const float theA, const float theB, const float theC, const float theD, const float theSerialResistor) {
  refResistor = theRefResistor;
  serialResistor = theSerialResistor;
  A = theA;
  B = theB;
  C = theC;
  D = theD;
  analogPinNbr = theAnalogPinNbr;
}

float ThermistorClient::readResistance()const {
  float sensorValue = analogRead(analogPinNbr);
  float V = sensorValue * (Vmax / (Nsteps - 1));
  DPRINTS(DBG_READ, "SensorValue=");
  DPRINT(DBG_READ, sensorValue);
  DPRINTS(DBG_READ, ", V=");
  DPRINT(DBG_READ, V);
  DPRINTS(DBG_READ, ", serial resistor=");
  DPRINT(DBG_READ, serialResistor);
  DPRINTS(DBG_READ, ", Vmax=");
  DPRINT(DBG_READ, Vmax);
  DPRINTS(DBG_READ, ", Nsteps=");
  DPRINTLN(DBG_READ, Nsteps);
  return (V * serialResistor) / (Vmax - V);
}

float ThermistorClient::readTemperature()const {
  float R = readResistance();
  DPRINTS(DBG_READ, "refResistor=");
  DPRINT(DBG_READ, refResistor);
  DPRINTS(DBG_READ, " R=");
  DPRINTLN(DBG_READ, R);
  float logRRref = log(R / refResistor);
  float temp = 1 / ((((D * logRRref + C) * logRRref + B ) * logRRref) + A);
  return temp - 273.15;
}
