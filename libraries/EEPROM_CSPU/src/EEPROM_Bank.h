/*
   EEPROM_Bank.h

   This is a subclass of EEPROM_BankWriter completed with methods required for reading.
*/

#pragma once
#include "EEPROM_BankWriter.h"

/** @ingroup EEPROM_CSPU
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class EEPROM_Bank : public EEPROM_BankWriter {
  public:
    // Overloaded methods
    EEPROM_Bank(const unsigned int theMaintenancePeriodInSec=10);
    /* Initialize everything according to available hardware.
       Assumes hw.getNumExternalEEPROM() > 0. */
    VIRTUAL bool init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize);

    /* Additionnal operation functions */
    /* This method should reset the pointer in ordre for the next call to readData() to return the first bytes written
       in the EEPROM (after the header).   */
    NON_VIRTUAL void resetReader();
    
    /** @brief Read the next dataSize bytes from the EEPROM Bank into data.
     *  If the data continues in the next chip, this methods reads the first part in the current chip and the second part in the next chip.
     *  It stops reading if it reaches the end of the data (or the end of the last chip).
     *  After reading, it returns the number of bytes actually read (less than the requested number if the end of the data or
     *  the end of the last chip is encountered.
     *  @param data The pointer to the first byte of the buffer to fill with the data from the EEPROM 
     *  @param dataSize The number of bytes to read (if 0 the function does not do anything).
     */
    NON_VIRTUAL byte readData(byte* data, const byte dataSize);
    /* This method checks that dataSize is equal to the record size (with an assertion) and then calls readData().
       It returns the number of bytes read, as received from readData().   */
    NON_VIRTUAL bool readOneRecord(byte* data, const byte dataSize);
    /* This method returns true if the end of the available data or the end of the last chip is reached, false otherwise */
    NON_VIRTUAL bool readerAtEnd();
    NON_VIRTUAL unsigned long leftToRead() const;
    NON_VIRTUAL unsigned long recordsLeftToRead() const;
  private:
    byte readerCurrentChip;
    EEPROM_Address readerCurrentAddress; // BUG!! était un unsigned int soit 8 bits sur Arduino.
};
