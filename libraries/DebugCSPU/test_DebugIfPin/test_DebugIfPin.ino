/* 
 *  test_DebugIfPin
 *  
 *  This sketch tests the DINIT_IF_PIN macro
 *  
 *  
 *  Wiring:  
 *     - pin ActivationPin: resistor to Vcc (=pulled up) 
 *     
 *  Usage:  run this sketch both with Arduino Uno and with another board which blocks if no terminal
 *          is actually connected to the USB Serial port, in each case, pull ActivationPin up or down
 *          
 *     On Arduino UNO: whatever the state of ActivationPin, the board should not block, and blink the
 *     on-board LED. 
 *     If a serial monitor is connected: at init, a message should confirm the state of the ActivationPin, and inform that
 *     the Serial port is not checked when the Activation pin is HIGH. 
 *     If no serial monitor is connected: the LED should blink. 
 *     
 *     On Feather M0 Express: 
 *        -With Serial monitor connected and  activationPin DOWN: Serial port should be properly initialized, 
 *         and every msg should appear before the LED blinks. 
 *        -With Serial monitor connected and  activationPin UP: Serial port should be properly initialized, 
 *         but program should start without waiting for the connexion to initialized: most setup messages are lost. 
 *        -Without a Serial monitor connected (power the board with battery or USB-Charger) and  activationPin DOWN: 
 *         The LED never starts blinking: the board is blocked waiting for the Serial connexion in the setup.
 *        -Without a Serial monitor connected and  activationPin UP: Serial port should be properly initialized, 
 *         but not checked, the board does not wait, and the LED is blinking. 
 *      
 *          
 *  
 *  With Uno: run the sketch 
 *
 *    
 */

#include "DebugCSPU.h"

constexpr byte ActivationPin=5; 
constexpr byte LedPin=LED_BUILTIN; 
constexpr byte activationPin=ActivationPin;
constexpr long int baudRate=115200; // TMP
  
 void setup() {
  #ifndef TMP
    DINIT_IF_PIN(115200, ActivationPin);
  
  #else 
  // DINIT(115200);
  Serial.begin(115200); 
  if(digitalRead(activationPin)==LOW) {    
         while (!Serial);                      
         Serial.println(F("Serial link OK"));  
  } else { Serial.println(F("Serial link not checked")); }
  #endif 
  Serial << "activation pin (#" << activationPin << ") is " 
         << ((digitalRead(activationPin)==LOW) ? "LOW" : "HIGH")  
         << ENDL; 
  pinMode(LedPin, OUTPUT);
  Serial << "Setup ok" << ENDL;
}

void loop() {
  digitalWrite(LedPin, !digitalRead(LedPin));
  Serial << "Running loop and blinking LED..." << ENDL;
  delay(1000);
}
