#include "SSC_GPS_Client.h"
#include "Adafruit_GPS.h"

#ifdef __AVR__
#include "SoftwareSerial.h"
SoftwareSerial mySerial(3 , 2);
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
HardwareSerial &mySerial = Serial1;
#endif
const bool detailedOutput = true; // Set to true to print all values on Serial
SSC_GPS_Client g(mySerial);
SSC_Record rec;

unsigned long lastCycleChange = 0;
unsigned long lastCycleLength = 0;

SSC_GPS_Client::Frequency frequenciesToTest[] = {SSC_GPS_Client::Frequency::F1Hz, SSC_GPS_Client::Frequency::F5Hz, SSC_GPS_Client::Frequency::F10Hz};
int testingFrequencyIndex = 0;
const long int changeTestingFrequencyEvery = 10000; //milliseconds
unsigned long int lastFrequencyChange = 0;

void setup() {
  DINIT(115200);
  Serial << "calling gps.begin..." << ENDL;
  //Pass parameter SSC_GPS_Client::Frequency::F...Hz where ... is 1, 5 or 10. If nothing is passed, 5 is default
  g.begin(frequenciesToTest[testingFrequencyIndex]);
  Serial << "Setup ok. " << ENDL;
}

void loop() {
  static unsigned long lastMillis = millis();
  rec.clear();
  rec.timestamp = millis();
  g.readData(rec);

  float currentLongitude = rec.GPS_LongitudeDegrees;
  float currentLatitude = rec.GPS_LatitudeDegrees;

  if (rec.newGPS_Measures) {
    lastCycleLength = millis() - lastCycleChange;
    lastCycleChange = millis();
  }

  unsigned long delta = rec.timestamp - lastMillis;
  if (detailedOutput) {
    Serial.print(rec.timestamp);
    Serial.print(":  Longitude:"); Serial.print(currentLongitude , 10); //prints the different parameters sent by the gps
    Serial.print(", Latitude:"); Serial.print(currentLatitude , 10);
    Serial.print(", Altitude:"); Serial.print(rec.GPS_Altitude , 10);
    Serial << "  DeltaT =" << delta << ENDL;
    rec.newGPS_Measures ? Serial.println("NEW GPS MEASURES") : Serial.println("No new GPS measures");
    Serial << "Testing frequency: " << (int)frequenciesToTest[testingFrequencyIndex] << " Hz" << ENDL;
    Serial << "Length of last cycle (time between the 2 last changes) (in milliseconds): " << lastCycleLength << ENDL;

  }
  if (currentLatitude < 45) Serial << "******** Abnormal value of latitude ********" << ENDL;
  if (currentLongitude < 3) Serial << "******** Abnormal Value of longitude ********" << ENDL;


  unsigned long expectedCycleDuration = 1000/((int)frequenciesToTest[testingFrequencyIndex]);

  
  if ((lastCycleLength > expectedCycleDuration + 50) || (lastCycleLength < expectedCycleDuration - 50)) {
    Serial << "******** Abnormal cycle length (" << (int)frequenciesToTest[testingFrequencyIndex] << "Hz) ********" << ENDL;
  }

  if (millis() - lastFrequencyChange >= changeTestingFrequencyEvery) {
    lastFrequencyChange = millis();
    testingFrequencyIndex = (testingFrequencyIndex + 1) % (sizeof(frequenciesToTest)/sizeof(*frequenciesToTest));
    g.changeFrequency(
    frequenciesToTest[testingFrequencyIndex]);
  }

  lastMillis = rec.timestamp;


  Serial << ENDL;

  delay(10); //test possibilities


}
