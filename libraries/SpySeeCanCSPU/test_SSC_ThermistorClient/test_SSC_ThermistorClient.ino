/*
   test_SSC_ThermistorClient
*/

#define DEBUG
#include "DebugCSPU.h"
#include "SSC_ThermistorClient.h"

SSC_ThermistorClient client;
SSC_Record record;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  record.clear();
  client.readData(record);
  record.print(Serial);
  delay(5000);
}
