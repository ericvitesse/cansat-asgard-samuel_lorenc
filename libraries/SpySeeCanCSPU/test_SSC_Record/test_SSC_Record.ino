/*
    test_SSC_Record.ino

    Test program for class SSC_Record.
*/
#include "SSC_Record.h" 
#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"

#define TEST_HUMAN_READABLE_PRINT  // undefine to avoid testing non operational code. 

unsigned int numErrors = 0;

void requestVisualCheck() {
  char answer = ' ';
  Serial << ENDL;
  // remove any previous character in input queue
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << "Is this ok (y/n) ? ";
    Serial.flush();
    // remove any previous character in input queue
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial << ENDL;
}

class SSC_Record_Test {
  public:
    SSC_Record_Test(SSC_Record& theRecord) : record(theRecord) {}
    void testIndividualPrints() {
      float testFloatArr[3] = {1.11, 2.22, 3.33};
      int16_t testIntArr[3] = {4, 5, 6};
      Serial << "Printing CSV Array values with NO final separator" << ENDL;
      record.printCSV(Serial, testFloatArr);
      requestVisualCheck();
      record.printCSV(Serial, testIntArr);
      requestVisualCheck();
      Serial << "Printing CSV Array values WITH final separator" << ENDL;
      record.printCSV(Serial, testFloatArr, true);
      requestVisualCheck();
      record.printCSV(Serial, testIntArr, true);
      requestVisualCheck();
      Serial << "Printing a CSV Float with NO final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3);
      requestVisualCheck();
      Serial << "Printing a CSV Value With a final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3, true);
      requestVisualCheck();
    }
    SSC_Record& record;
} ;

SSC_Record record;

void assignValues (SSC_Record& rec) {
  rec.timestamp = 1;
  
  rec.newGPS_Measures = true;
  rec.GPS_LatitudeDegrees = 25.25;
  rec.GPS_LongitudeDegrees = 26.26;
  rec.GPS_Altitude = 27.27;
#ifdef INCLUDE_GPS_VELOCITY
  rec.GPS_VelocityKnots = 28.28;
  rec.GPS_VelocityAngleDegrees = 29.29;
#endif
  
  rec.temperatureBMP = 36.36;
  rec.pressure = 37.37;
  rec.altitude = 38.38;
  rec.temperatureThermistor1 = 39.39;
  rec.temperatureThermistor2 = 40.40;
  rec.temperatureThermistor3 = 41.41;
}

void performTest() {  
  assignValues(record);
  SSC_Record_Test friendClass(record);
  friendClass.testIndividualPrints();

  Serial << "Values Assigned (printing complete record in CSV format (with header):" << ENDL << "   ";
  record.printCSV_Header(Serial);
  Serial << ENDL << "   ";
  record.printCSV(Serial);
  requestVisualCheck();

  Serial << ENDL << "Testing SSC_Record Clear(): " << ENDL << "   ";
  record.clear();
  record.printCSV(Serial);
  Serial << ENDL;
  DASSERT(record.accelRaw[0] == 0); DASSERT(record.accelRaw[1] == 0); DASSERT(record.accelRaw[2] == 0);
  DASSERT(record.gyroRaw[0] == 0); DASSERT(record.gyroRaw[1] == 0); DASSERT(record.gyroRaw[2] == 0);
  DASSERT(record.mag[0] == 0); DASSERT(record.mag[1] == 0); DASSERT(record.mag[2] == 0);
  Serial << "   Values checked. " << ENDL;

#ifdef TEST_HUMAN_READABLE_PRINT
  assignValues(record);
  Serial << "Printing complete record in Human Readable Format:" << ENDL << "   ";
  record.print(Serial);
  Serial << ENDL;
  requestVisualCheck();
#endif
  Serial << "End of job. Total number of errors:" << numErrors <<  ENDL;
  Serial << "Size of SSC_Record is " << sizeof(SSC_Record) << " bytes." ;
}

void setup() {
  Serial.begin(115200);
  while (!Serial);
  assignValues(record);
  performTest();
}

void loop() {}
