#include "Arduino.h"
#include "SSC_Record.h"
#include "IsaTwoInterface.h"

#define DEBUG
#include <DebugCSPU.h>
#define DBG 1

SSC_Record::SSC_Record () {
  SSC_Record::clear();
}

void SSC_Record::clear() {
  timestamp = 0;

  //A. GPS DATA
  newGPS_Measures = false;
  GPS_LatitudeDegrees = 0.00;
  GPS_LongitudeDegrees = 0.00;
  GPS_Altitude = 0.00;
#ifdef INCLUDE_GPS_VELOCITY
  GPS_VelocityKnots = 0.00;
  GPS_VelocityAngleDegrees = 0.00;
#endif


  temperatureBMP = 0.00;
  pressure = 0.00;
  altitude = 0.00;
  temperatureThermistor1 = 0.00;
  temperatureThermistor2 = 0.00;
  temperatureThermistor3 = 0.00;

  //D. ADD HERE SECONDARY MISSION DATA AND VARIABLES ONCE AVAILABLE
   

}
void SSC_Record::clearArray(float arr[3]) {
  for (int i = 0; i < 3; i++) {
    arr[i] = 0;
  }
}
void SSC_Record::clearArray(int16_t arr[3]) {
  for (int i = 0; i < 3; i++) {
    arr[i] = 0;
  }
}
void SSC_Record::printCSV(Stream& str, const float arr[3], bool finalSeparator) const {
  str.print(arr[0], numDecimalPositionsToUse); str << separator;
  str.print(arr[1], numDecimalPositionsToUse); str << separator;
  str.print(arr[2], numDecimalPositionsToUse);
  if (finalSeparator) { 
    str << separator;
  }
}
void SSC_Record::printCSV(Stream& str, const int16_t arr[3], bool finalSeparator) const {
  str << arr[0] << separator << arr[1] << separator << arr[2];
  if (finalSeparator) {
    str << separator;
  }
}

void SSC_Record::printCSV(Stream& str, const float &f, bool finalSeparator) const {
  str.print(f, numDecimalPositionsToUse);
  if (finalSeparator) {
    str << separator;
  }
}

void SSC_Record::printCSV(Stream& str, const bool b, bool finalSeparator) const {
  str.print((int) b);
  if (finalSeparator) {
    str << separator;
  }
}

void SSC_Record::printCSV_Header(Stream& str, DataSelector select) const {
  str << F( "type,timestamp,");
  if (select == DataSelector::All) {
    printCSV_HeaderPart(str, DataSelector::GPS, true);
    printCSV_HeaderPart(str, DataSelector::PrimarySecondary, false);
  }
  else {
    printCSV_HeaderPart(str, select, false);
  }
}

void SSC_Record::printCSV_HeaderPart(Stream& str, DataSelector select, bool finalSeparator) const {
  switch (select) {
    case DataSelector::GPS :
      str << F("GPS_Measures,GPS_LatitudeDegrees,");
      str << F("GPS_LongitudeDegrees,GPS_Altitude");
#ifdef INCLUDE_GPS_VELOCITY
      str << F(",GPS_VelocityKnots,GPS_VelocityAngleDegrees");
#endif

      break;
    case DataSelector::PrimarySecondary :
      str << F("temperatureBMP,pressure,altitude,temperatureThermistor1,temperatureThermistor2,temperatureThermistor3,");
      str << F("TBC");
      break;
    default:
      DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
      DASSERT (false);
  }
  if (finalSeparator) {
    str << separator;
  }
}

void SSC_Record::printCSV_Part(Stream& str, DataSelector select, bool finalSeparator) const {
  switch (select) {
    case DataSelector::GPS :
      printCSV(str, newGPS_Measures, true);
      printCSV(str, GPS_LatitudeDegrees, true);
      printCSV(str, GPS_LongitudeDegrees, true);
#ifdef INCLUDE_GPS_VELOCITY
      printCSV(str, GPS_Altitude, true);
      printCSV(str, GPS_VelocityKnots, true);
      printCSV(str, GPS_VelocityAngleDegrees, false);
#else
      printCSV(str, GPS_Altitude, false);
#endif
      if (finalSeparator) {
        str << separator;
      }
      break;

    case DataSelector::PrimarySecondary :
      printCSV(str, temperatureBMP, true);
      printCSV(str, pressure, true);
      printCSV(str, altitude, true);
      printCSV(str, temperatureThermistor1, true);
	  printCSV(str, temperatureThermistor2, true);
	  printCSV(str, temperatureThermistor3, true);
      printCSV(str, 0.0f, false);
      if (finalSeparator) {
        str << separator;
      }
      break;

    default:
      DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
      DASSERT (false);
  }
}
void SSC_Record::printCSV(Stream& str, DataSelector select) const {
  str <<  (uint16_t) IsaTwoRecordType::DataRecord << separator << timestamp << separator;
  if (select == DataSelector::All) {
    printCSV_Part(str, DataSelector::GPS, true);
    printCSV_Part(str, DataSelector::PrimarySecondary, false);
  }
  else {
    printCSV_Part(str, select, false);
  }
}
