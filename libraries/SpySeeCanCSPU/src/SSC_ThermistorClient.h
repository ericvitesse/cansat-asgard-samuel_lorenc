/*
   SSC_ThermistorClient.h
*/

#pragma once
#include "SSC_Record.h"
#include "NTCLE100E3_Client.h"
#include "NTCLG100E2_Client.h"
#include "VMA320_Client.h"

/** @ingroup SpySeeCanCSPU
    @brief 
*/

class SSC_ThermistorClient {
  public:
    SSC_ThermistorClient();
    bool readData(SSC_Record& record);

  private:
    NTCLE100E3_Client thermistor1;
    NTCLG100E2_Client thermistor2;
    VMA320_Client thermistor3;


};
