#include <Adafruit_GPS.h>
#ifdef __AVR__
#include <SoftwareSerial.h>
#endif
#include "SSC_Record.h"

/**
   @ingroup SpySeeCanCSPU
   @brief This class aquires the following data from the gps: latitude, longitude, altitude, velocity (direction and angular speed).
   Latitude & longitude are expressed in the WGS_1984 refential. Altitude is relative to the mean sea level (geoid).
   The data sent by the gps at a frequency defined in the call of begin() will be then stored in an SSC_Record.
*/
class SSC_GPS_Client {
  public:
    /**
         @param  serialPort The serial port to which the GPS is physically connected.
    */
    /**The feather m0 not supporting a software serial, there has to be a clause that assigns a hardwareSerial to the class if the feather is used*/
#ifdef __AVR__
    SSC_GPS_Client(SoftwareSerial& serialPort);
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
    SSC_GPS_Client(HardwareSerial& serialPort);

#endif
    /** Enum values for possible frequencies */
    enum class Frequency {
      // Developer note: make sure value is the frequency in hz to allow for casting to integer
      F1Hz = 1, /**< Set frequency to 1Hz */
      F5Hz = 5, /**< Set frequency to 5Hz */
      F10Hz = 10 /**< Set frequency to 10Hz */
    };
    
    /**
      @brief Call this method once in the the setup to configure the GPS device.
      @param updateFrequency The desired update frequency (Frequency::F1Hz, Frequency::F5Hz, or Frequency::F10Hz). Default is Frequency::F5Hz.
    */
    void begin(const Frequency updateFrequency = Frequency::F5Hz);

    /**
        @brief Change GPS update frequency
        @param updateFrequency The desired update frequency (Frequency::F1Hz, Frequency::F5Hz, or Frequency::F10Hz)
    */
    void changeFrequency(const Frequency updateFrequency);

    /**
        @brief Acquire GPS data and store in provided record
        @param record The SSC record in which fields GPS_LongitudeDegrees,GPS_LatitudeDegrees,GPS_Altitude,(GPS_VelocityKnots,GPS_VelocityAngleDegrees) are filled with GPS readings. newGPS_Measures field is also updated.
    */
    void readData(SSC_Record &record);
   
  private:
    /**< The Adafruit gps driver */
    Adafruit_GPS myGps;
    float lastLongitudeValue; /**< Last recorded longitude */
    float lastLatitudeValue; /**< Last recorded latitude */
};
