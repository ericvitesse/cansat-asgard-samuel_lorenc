/*
   SSC_XbeeClient.h
*/

#include "XBeeClient.h"
#include "SSC_Record.h"

#pragma once

#undef XBEECLIENT_INCLUDES_UTILITIES
constexpr uint8_t SSC_RecordType = 12;

/**
  @ingroup SpySeeCanCSPU
  @brief This class is a gateway to and from the XBee module, and provides services to send and receive SSC_Records.
  It delegates the actual transfer to its XBee_Client superclass.
*/

class SSC_XBeeClient : public XBeeClient {

  public:
    /**Build up constructor - ATTENTION, values must be given in hexa (ex: 0x0013a200)
         @param SH in the Serial Number High specific to the receiving Xbee
         @param SL is the Serial Number Low specific to the receiving Xbee */

    SSC_XBeeClient(uint32_t SH, uint32_t SL): XBeeClient(SH, SL) {};
    /** Initialize point to point radio link
        @param stream The serial object to use to communicate with
                the XBee module. */
    virtual ~SSC_XBeeClient() {};
    /**Send a data record.
       @param record, The payload to transfer
       @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.

       @return True if transfer is successful, false otherwise
    */
    virtual bool send(const SSC_Record& record,  int timeOutCheckResponse = 0);

#ifdef XBEECLIENT_INCLUDES_UTILITIES
    /** This version just intercept calls to superclass' send(), in order to display
        debug information.
    */
    virtual bool send(	uint8_t* data, uint8_t dataSize,
                        int timeOutCheckResponse = 300,
                        uint32_t SH = 0, uint32_t SL = 0);
    void displayFrame(uint8_t* data, uint8_t dataSize);
#endif
    /** Receive a single SSC_Record
        @param incomingRecord The record to configure with the data received. Its content is
                              overwritten.
        @return true if a record was received, false if nothing are anything else than
                a valid record was received.
    */
    virtual bool receiveRecord(SSC_Record& incomingRecord);
  
  protected:

    static constexpr bool displayOutgoingFramesOnSerial = false;
    
      /** Extract data from the payload to fill an SSC_Record.
       @param record The record to fill with the data
       &param data Payload pointer
       @param dataSize Payload size
       @pre The payload is expected to be exactly a byte with value
          SSC_RecordType::DataRecord, an unused byte and
            an SSC_Record.
       @return True if operation was successful, false otherwise.
    */
    bool getDataRecord(SSC_Record& record, const uint8_t* data, uint8_t dataSize);
 
};
