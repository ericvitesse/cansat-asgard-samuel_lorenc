#pragma once

#include "Arduino.h"
#include "SSC_Config.h"

#undef INCLUDE_GPS_VELOCITY   // Define to transport GPS velocity data.

/** @ingroup SpySeeCanCSPU
    @brief The record carrying all data acquired or computed by the CanSat.
    Its record type must be IsaTwoRecordType::DataRecord (cf. IsaTwoInterface.h) to distinguish from commands.
*/

class SSC_Record  {
  public:

    SSC_Record ();

    /** Enum values, allowing for restricting the CSV output to a subset of the record */
    enum class DataSelector {
      All,         /**< Select complete record */
      GPS,    /**< Select data from the GPS modules only */
      PrimarySecondary /**< Select data from primary and secondary sensors only */
    } ;

    /** Stream the record in human-readable format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                   complete records, if the data must be parsed by code expecting a SSC_Record).
    */
    void print(Stream& str, DataSelector select = DataSelector::All) const;
    /** Stream the record in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a SSC_Record).
    */
    void printCSV(Stream& str, DataSelector select = DataSelector::All) const;
    /** Stream the header line in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a SSC_Record).
    */
    void printCSV_Header(Stream& str, DataSelector select = DataSelector::All) const;

    /** Set all values to 0 or false */
    void clear();

    // Data members
    unsigned long timestamp{};  /**< Record timestamp in msec. */

    // A. GPS data
    bool  newGPS_Measures;        /**< true if GPS data is included in the record */
    float GPS_LatitudeDegrees;    /**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
    float GPS_LongitudeDegrees;   /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
    float GPS_Altitude;           /**< Altitude of antenna, in meters above mean sea level (geoid) */
#ifdef INCLUDE_GPS_VELOCITY
    float GPS_VelocityKnots;      /**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
    float GPS_VelocityAngleDegrees; /**< Direction of velocity in decimal degrees, 0 = North */
#endif

    // B. Primary mission data
    float temperatureBMP;    /**< The temperature in °C gathered from the BMP */
    float pressure;       /**< The pressure in hPA */
    float altitude;       /**< The altitude (m) as derived from the pressure */
    float temperatureThermistor1;    /**< The temperature in °C gathered from the first Thermistor*/
	float temperatureThermistor2;    /**< The temperature in °C gathered from the second Thermistor*/
	float temperatureThermistor3;    /**< The temperature in °C gathered from the third Thermistor*/

    // C. Secondary mission data
   // Secundary mission data TO BE COMPLETED
  protected:
    /** Utility function: print an array of 3 numbers in CSV format, using the right number of
        decimal positions, from IsaTwoConfig.h
        @param str The output stream
        @param arr The array of 3 numbers to print.
    */
    void printCSV(Stream& str, const float arr[3], bool finalSeparator = false) const;
    /** @copydoc printCSV(Stream& str, const float arr[3], bool finalSeparator = false) const */
    void printCSV(Stream& str, const int16_t arr[3], bool finalSeparator = false) const;

    /** Utility function: print a float, using the right number of decimal positions, from
        IsaTwoConfig.h, with a final comma, if finalSeparator is true.
    */
    void printCSV(Stream& str, const float &f, bool finalSeparator = false) const;

    /** Utility function: print an uint16_t,  with a final comma, if finalSeparator is true.
     */
     void printCSV(Stream& str, const uint16_t &val, bool finalSeparator = false) const
     {
    	 str << val;
     	 if (finalSeparator) {
     		 str << separator;
     	 }
     }

    /** Utility function: print a bool as 1 or 0, with a final comma, if finalSeparator is true.
     */
     void printCSV(Stream& str, const bool b, bool finalSeparator = false) const;

    /** Utility function: print an array of 3 numbers in human-readable format, using the right
        number of decimal positions, from SSC_Config.h
        @param str The output stream
        @param arr The array of 3 numbers to print.
    */
    void print(Stream& str, const float arr[3]) const;
    /** @copydoc print(Stream& str, const float arr[3]) const */
    void print(Stream& str, const int16_t arr[3]) const;
    /** Utility function: set an array of 3 numbers to 0
        @param arr The array of 3 numbers to print.
    */
    void clearArray(float arr[3]);
    /** @copydoc clearArray(float arr[3]) */
    void clearArray(int16_t arr[3]);

    void printCSV_HeaderPart(Stream& str, DataSelector select, bool finalSeparator) const;
    void printCSV_Part(Stream & str, DataSelector select, bool finalSeparator) const;
    void print_Part(Stream & str, DataSelector select) const;
private:
    static constexpr char separator = ',';
    friend class SSC_Record_Test;
} ;
