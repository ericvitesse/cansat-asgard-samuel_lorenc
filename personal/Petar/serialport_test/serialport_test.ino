#define DEBUG //use this in front of the include to debug
#include "DebugCSPU.h"

#define DBG_SOMETHING 1
int a;

void setup() {
  DINIT(115200); //pour ininialiser le debug
  a=0;
  Serial << "Setup OK \n"; //Cette syntaxe: que pour les programmes de test
}

void loop() {
  DPRINTS(DBG_SOMETHING, "a=");
  DPRINT(DBG_SOMETHING, a);
  DPRINTSLN(DBG_SOMETHING, ".");
  a++;
  delay(5000);
}
