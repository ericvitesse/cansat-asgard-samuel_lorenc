
OO_Sos::OO_Sos(byte pinNumber) {
  pinMode(pinNumber, OUTPUT);
  currentChar = 0;
  currentSign = 0;
  currentSignStep = 0;
  elapsed = 0;
}

// Travaille sur le signe demandé, renvoie true si il a fini.
bool OO_Sos::runSign(unsigned int duration) {
  bool done=false;
  switch (currentSignStep) {
    case '0': // pas commencé
      digitalWrite(pinNumber, HIGH);
      currentSignStep = 1;
      elapsed = 0;
      break;
    case '1': // c'est allumé
      if (elapsed >= duration) {
        digitalWrite(pinNumber, LOW);
        currentSignStep = 2;
        elapsed = 0;
      }
      break;
    case '2': // on attend
      if (elapsed >= WaitAfterSign) {
          currentSignStep=0;
          done=true;
      }
     default:
        ERREUR
  }
  return done;
}

/* Travaille sur le caractère courant, renvoie true si il a terminé le caractère */
bool OO_Sos::runCharacter() {
  bool done = false;
  unsigned long signDuration;
  switch (currentCharacter) {
    case '0':
    case '1':
      signDuration = DotDuration;
      break;
    case '2':
      signDuration = DashDuration;
      break;
    default:
      ERREUR.
  }
  if (runSign(signDuration)) {
    currentSign++;
    if (currentSign > 3) {
      done = true;
    }
  }
  return done;
}

void OO_Sos::run() {
  if (runCharacter()) {
    currentChar++;
    currentChar = currentChar % 3;
  }
}
