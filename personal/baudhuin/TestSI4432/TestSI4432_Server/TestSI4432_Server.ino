/* rf22_server.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing server
// with the RF22 class. RF22 class does not provide for addressing or reliability.
// It is designed to work with the other example rf22_client
// Connexions: Use a level shifter with Arduino !
      VCC - 3V (!! 5V not supported, max 3.6V)
      GND - GND
      SDO - MISO=12
      SDI - MOSI=11
      SCLK- SCK=13
      nSEL- 10 
      SDN - GND 
      nIRQ - 5  
*/

#define DEBUG     // To activate debugging functions in DebugCSPU.h
#include "DebugCSPU.h"
#include <RH_RF22.h>

const byte CS_Pin=10;                   // Digital pin used as Chip-Select line for the SS4432
const byte InterruptPin=5;              // Digital pin used by the SS4432 to trigger interruption


// Singleton instance of the radio
RH_RF22 rf22(CS_Pin,InterruptPin);

void setup() 
{
  DINIT(115200);
  Serial << "*** TestSI4432_Server **** " << ENDL;
  delay(1000);
  Serial.println("Set up beginning...");
  Serial.flush();
  
  while (!rf22.init()) {
    Serial.println("RF22 init failed. Retrying....");
    Serial.flush();
    delay(1000);
  }
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  Serial.println("Setup OK");}

void loop()
{
  while (1)
  {
    Serial << "Pending on input queue..." << ENDL;
    rf22.waitAvailable();
    
    // Should be a message for us now   
    uint8_t buf[RH_RF22_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf22.recv(buf, &len))
    {
      Serial.print("got request: ");
      Serial.println((char*)buf);
      
      // Send a reply
      uint8_t data[] = "And hello back to you";
      rf22.send(data, sizeof(data));
      rf22.waitPacketSent();
      Serial.println("Sent a reply");
    }
    else
    {
      Serial.println("recv failed");
    }
  }
}
