

void setup() {

  Serial.begin(19200);
  unsigned long b = 5;
  Serial.print("b = ");
  Serial.println(b);

  uint16_t i = 65534;
  Serial.print("i = ");
  Serial.println(i);

  i += 1;
  Serial.print("On the limit of unsigned int... i = ");
  Serial.println(i);

  i += 1;
  Serial.print("With supposed ovf... i = ");
  Serial.println(i);
  Serial.flush();

  b += (unsigned long) i + 1;
  Serial.print("b with ovf of i... i = ");
  Serial.println(b);
}

void loop() {

}
