/**CAN SIDE
 * test_XBeeClient with a real size record
 * 
 */


#include "XBeeClient.h"
#include "IsaTwoRecord.h"
#define DEBUG
#include "DebugCSPU.h"
#define DBG_TEST 0

XBeeClient xbee(13a200,);
IsaTwoRecord record;

HardwareSerial &RF = Serial1;
int counter = 0;
String message = "Echo comamnd";

// Do not use more than 7 digits for the floats.
#define randomFloat1 123.4567
#define randomFloat2 234.5678
#define randomFloat3 345.6789
#define randomFloat4 456.7891
#define randomFloat5 567.8912
#define randomFloat6 678.9123
#define randomInt1 12345
#define randomInt2 23456
#define randomInt3 34567
#define randomInt4 45678
#define randomInt5 56789
#define randomInt6 67891

void initRecord(IsaTwoRecord &rec) {
  // Put some realistic data in the record to make sure it has a realistic length.
  rec.clear();
  rec.GPS_Altitude = randomFloat1;
  rec.GPS_LatitudeDegrees = randomFloat2;
  rec.GPS_LongitudeDegrees = randomFloat3;
  rec.accelRaw[0] = randomInt1;
  rec.accelRaw[1] = randomInt2;
  rec.accelRaw[2] = randomInt3;
  rec.gyroRaw[0] = randomInt4;
  rec.gyroRaw[1] = randomInt5;
  rec.gyroRaw[2] = randomInt6;
  rec.mag[0] = randomFloat4;
  rec.mag[1] = randomFloat5;
  rec.mag[2] = randomFloat6;
  rec.altitude = randomFloat1;
  rec.co2_voltage = randomFloat2;
  rec.newGPS_Measures = true;
  rec.pressure = randomFloat3;
  rec.temperatureBMP = rec.temperatureThermistor = randomFloat4;
}

void setup() {
  Serial.begin(115200);
  RF.begin(115200);
  xbee.begin(RF);
  initRecord(record);
}

void loop() {
  bool receiving = xbee.receive(payLoad, payLoadSize);
  if (receiving) {
    DPRINTS(DBG_TEST, "We received something...Most likely a command");
    for (int i = 0; i < payLoadSize; i++) {
      Serial << payLoad[i];
    }
    Serial << ENDL;
    xbee.send(message, sizeof(message), 30);
  }
  if (
    bool sending = xbee.send(record, sizeof(record), 30);
  if (sending) {
    Serial << ".";
    counter++;
    if (counter == 25) {
        Serial << ENDL;
        counter  = 0;
      }
    } else {
      Serial << "***Couldn't send the record***" << ENDL;
    }
}
