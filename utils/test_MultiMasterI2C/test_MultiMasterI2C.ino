
/**
*
* Sample Multi Master I2C implementation.  Sends a button state over I2C to another
* Arduino, which flashes an LED correspinding to button state.
*
* Connections: Arduino analog pins 4 and 5 are connected between the two Arduinos,
* with a 1k pullup resistor connected to each line.  Connect a push button between
* digital pin 10 and ground, and an LED (with a resistor) to digital pin 9.
*
*/

#include <Wire.h>
#define DEBUG
#include "DebugCSPU.h"
#include "LSM_Client.h"

#define LED 9
#define BUTTON 10

#define THIS_ADDRESS 0x8
#define OTHER_ADDRESS 0x9

boolean last_state = HIGH;

void setup() {
 Serial.begin(115200);
 while (!Serial) ;

 Serial.print("Multi-Master I2C test. My adress = ");
 Serial.print(THIS_ADDRESS);
 Serial.print(", other adress=");
 Serial.println(OTHER_ADDRESS);
 
 pinMode(LED, OUTPUT);
 digitalWrite(LED, LOW);
 
 pinMode(BUTTON, INPUT);
 digitalWrite(BUTTON, HIGH);
 
  Wire.begin();
}

void loop() {
 Serial.print('.');
 if (1){
   last_state = !last_state;
  // Serial.print("Transmitting ");
  // Serial.println(last_state);
   
   Wire.beginTransmission(OTHER_ADDRESS);
   Wire.write(last_state);
   byte result=Wire.endTransmission();
   if (result!=0) {
    Serial.print("Error in transmission: " );
    Serial.println(result);
   }
 }
 delay(100); 
}

void receiveEvent(int howMany){
 while (Wire.available() > 0){
   boolean b = Wire.read();
   Serial.print(b, DEC);
 }
 Serial.println();
}
