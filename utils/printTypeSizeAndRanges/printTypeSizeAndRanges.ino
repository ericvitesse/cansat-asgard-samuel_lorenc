#include <limits.h>
#include <float.h>
#include "SerialStream.h"

void printTypeInfo(const __FlashStringHelper* label, long theSize,  unsigned long theMin, unsigned long theMax)
{
  Serial << F("  ") << label << F(": size=") << theSize
         << F(", range=[") << theMin << F(";") << theMax << F("]") << ENDL;
}
void printTypeInfo(const __FlashStringHelper* label, long theSize,  long theMin,  long theMax)
{
  Serial << F("  ") << label << F(": size=") << theSize
         << F(", range=[") << theMin << F(";") << theMax << F("]") << ENDL;
}
void printTypeInfo(const __FlashStringHelper* label, long theSize,  double theMin, double theMax)
{
  Serial << F("  ") << label << F(": size=") << theSize
         << F(", range=[") << theMin << F(";") << theMax << F("]") << ENDL;
}

typedef struct IsatisRecordMinimum {
  unsigned long timeInMilliSeconds;
  double GPY_OutputV;
  float BMP280_Pressure;
  float BMP280_Altitude;
  float BMP280_Temperature;

} IsatisRecordMinimum ;

typedef struct IsatisRecordComplete {
  unsigned long timeInMilliSeconds;
  double GPY_OutputV;
  double GPY_uqm3;
  double GPY_aqi;
  float BMP280_Pressure;
  float BMP280_Altitude;
  float BMP280_Temperature;

} IsatisRecordComplete ;

const unsigned long totalSize = 6 * 32L;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(19200);
  while (!Serial) ;

  Serial << F("Size of the various types on this Arduino board:") << ENDL;
  printTypeInfo(F("char          "), sizeof(char), (long) CHAR_MIN, (long)CHAR_MAX);
  printTypeInfo(F("byte          "), sizeof(byte), 0, (long)UCHAR_MAX);
  printTypeInfo(F("short         "), sizeof(short), (long) SHRT_MIN, (long) SHRT_MAX);
  printTypeInfo(F("unsigned short"), sizeof(short), (unsigned long) 0, USHRT_MAX);
  printTypeInfo(F("int           "), sizeof(int), (long) INT_MIN, (long) INT_MAX);
  printTypeInfo(F("uint          "), sizeof(int), (unsigned long) 0, UINT_MAX);
  printTypeInfo(F("long int      "), sizeof(long), LONG_MIN, LONG_MAX);
  printTypeInfo(F("float         "), sizeof(float), -FLT_MAX, FLT_MAX);
  printTypeInfo(F("double        "), sizeof(double), -DBL_MAX, DBL_MAX);
  printTypeInfo(F("Isatis minim. "), sizeof(IsatisRecordMinimum), 0L, 0L);
  printTypeInfo(F("Isatis compl. "), sizeof(IsatisRecordComplete), 0L, 0L);

  long numComplete = (totalSize * 1024L - 25) / sizeof(IsatisRecordComplete);
  long numMinimum = (totalSize * 1024L - 25) / sizeof(IsatisRecordMinimum);
  Serial << ENDL <<  F("Number of records in ") << totalSize << F("kb") << ENDL;
  Serial << F("Complete records: ") << numComplete << ENDL;
  Serial << F("Mimimum records: ") << numMinimum << ENDL;
  Serial << ENDL ;
  Serial <<  F("Time if 1 complete mesure/sec: ") << numComplete / 60L << F(" minutes") << ENDL;
  Serial << F("Time if 1 minimum  mesure/sec: ") << numMinimum / 60L << F(" minutes") << ENDL;

}

void loop() {
  // put your main code here, to run repeatedly:

}
