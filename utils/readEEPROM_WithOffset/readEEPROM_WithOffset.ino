/*
    This sketch reads numRecords IsatisDataRecords from startAddress, with increasing offset, in chip chipId
*/

#include "ExtEEPROM.h"
#include "SerialStream.h"
#include "Wire.h"
#include "IsatisDataRecord.h"

// MAUD, change the following constants as needed.
const unsigned long startAddress=0x0000;
const byte chipId=0;
const unsigned long numRecords=20; 
const byte firstOffset=0;
const byte lastOffset=sizeof(IsatisDataRecord)-1;

// Should not be necessary to change anything below this point. 
const unsigned long chipLastAddress=0xffff;
const byte recSize=sizeof(IsatisDataRecord);
const byte I2C_Address=0x50+chipId;

void dumpEEPROM(byte offset)
{
  IsatisDataRecord rec;
  unsigned long address=startAddress+ offset;
  unsigned long counter=0L;


 Serial << ENDL << F("Reading from address ");
 Serial.print(startAddress, HEX); 
 Serial << F(" with offset ") << offset << ENDL;
 Serial << F("------------------------------") << ENDL;
 
  while ((counter < numRecords) && ((address+recSize) <= chipLastAddress))
  {
     byte read = ExtEEPROM::readData(I2C_Address, address, (char *) (&rec), recSize);
    if (read != recSize) {
      Serial << F("*** Error: read ") << read << F(" bytes instead of ") << recSize << ENDL;
      break;
    }
    rec.printCSV(Serial);
    Serial<<ENDL;
    address += read;
    counter++;
  }
}

void setup() {
  Serial.begin(19200);
  while (!Serial) ;
  Wire.begin();
  Wire.setClock(400000L);
}

void loop() {
  for (byte offset=firstOffset; offset<lastOffset; offset++) {
    dumpEEPROM(offset);
  }
  Serial << F("End of job.") << ENDL << ENDL;
}
